from rest_framework.viewsets import ReadOnlyModelViewSet, mixins, GenericViewSet
from rest_framework.views import APIView
from rest_framework.generics import get_object_or_404

from rest_framework import permissions

from rest_framework.response import Response

from .permissions import AuthorPermission

from .models import Book, Author, PublishingHouse, Comment
from .serializer import BookSerializer, AuthorSerializer, PHSerializer, CommentCreateSerializer

from .docs import doc_renders

class BookViewSet(ReadOnlyModelViewSet):

    queryset = Book.objects.all()
    serializer_class = BookSerializer

class BookDocInfoView(APIView):

    def get(self, request):
        renderer = doc_renders.BookInfoRender()
        books = BookSerializer(Book.objects.all(), many=True)
        docx_name = renderer.render(books.data)
        return Response({"file_name":docx_name})

class AuthorViewSet(ReadOnlyModelViewSet):

    queryset = Author.objects.all()
    serializer_class = AuthorSerializer

class PHViewSet(ReadOnlyModelViewSet):

    queryset = PublishingHouse.objects.all()
    serializer_class = PHSerializer

class CommentCreateView(mixins.CreateModelMixin,
                        GenericViewSet):

    queryset = Comment.objects.all()
    serializer_class = CommentCreateSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class AuthorRegisterView(APIView):

    def get_permissions(self):
        if self.http_method_names == "DELETE":
            return [AuthorPermission()]
        else:
            return [permissions.IsAuthenticated()]

    def post(self, request):
        username = request.user.username
        if Author.objects.filter(name=username):
            return Response({"detail" : "Your Author"})
        author = Author.objects.create(name=username)
        return Response(AuthorSerializer(author).data)

    def delete(self, request):
        username = request.user.username
        author = get_object_or_404(Author.objects.all(), name = username)
        author.delete()
        return Response({"detail":"Author Status Delete"})

