from rest_framework.routers import DefaultRouter
from rest_framework.urls import url

from .views import BookViewSet, AuthorViewSet, PHViewSet, CommentCreateView, AuthorRegisterView, BookDocInfoView

router = DefaultRouter()

router.register(r'books', BookViewSet, basename='books')

router.register(r'authors', AuthorViewSet, basename='authors')

router.register(r'publishing_houses', PHViewSet, basename='publishing_houses')

router.register(r'comments', CommentCreateView, basename='comments')


urlpatterns = router.urls

urlpatterns += [
    url('authors/became_an_author', AuthorRegisterView.as_view(), name='became an author'),
    url('docs/', BookDocInfoView.as_view(), name="doc info")
]