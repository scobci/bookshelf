from rest_framework import serializers
from .models import Book, Author, PublishingHouse, Comment

class CommentFilterSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        data = data.filter(parent=None)
        return super().to_representation(data)

class CommentTreeSerializer(serializers.Serializer):
    def to_representation(self, instance):
        serializer = self.parent.parent.__class__(instance, context=self.context)
        return serializer.data

class CommentSerializer(serializers.ModelSerializer):

    children = CommentTreeSerializer(many=True)

    book = serializers.SlugRelatedField(slug_field="title",
                                        read_only=True)
    class Meta:
        list_serializer_class = CommentFilterSerializer
        model = Comment
        fields = ('id',
                  'user_name',
                  'text',
                  'book',
                  'children')

class CommentCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'

class BookSerializer(serializers.ModelSerializer):

    author = serializers.SlugRelatedField(slug_field="name",
                                          read_only=True)
    publishing_house = serializers.SlugRelatedField(slug_field="name",
                                          read_only=True)
    comments = CommentSerializer(many=True)
    class Meta:
        model = Book
        fields = ('id',
                  'title',
                  'annotation',
                  'url',
                  'author',
                  'publishing_house',
                  'comments',
                  'book_cover')


class AuthorSerializer(serializers.ModelSerializer):
    books = BookSerializer(many=True)
    class Meta:
        model = Author
        fields = ('id',
                  'name',
                  'books')


class PHSerializer(serializers.ModelSerializer):

    books = BookSerializer(many=True)
    class Meta:
        model = PublishingHouse
        fields = ('id',
                  'name',
                  'address',
                  'books')
