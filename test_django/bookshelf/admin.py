from django.contrib import admin

from .models import Author, PublishingHouse, Book

admin.site.register(Author)
admin.site.register(PublishingHouse)
admin.site.register(Book)
