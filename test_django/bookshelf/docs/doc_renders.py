from docxtpl import DocxTemplate
import os

class BookInfoRender():

    __template_path = os.path.abspath('bookshelf/docs/templates/info_book.docx')

    def render(self, books):

        doc = DocxTemplate(BookInfoRender.__template_path)
        table = doc.tables[0]

        for i, book in enumerate(books):
            index = i+1
            table.add_row()
            table.cell(index, 0).text = book['title']
            table.cell(index, 1).text = book['annotation']
            table.cell(index, 2).text = book['author']
            table.cell(index, 3).text = book['publishing_house']

        result_path = os.path.abspath('bookshelf/docs/book_info/' + "info_books.docx")
        doc.save(result_path)

        return "info_books.docx"