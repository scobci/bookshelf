from django.db import models
from django.contrib.auth.models import User

class Author(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class PublishingHouse(models.Model):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=512)

    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=255)
    annotation = models.TextField()
    url = models.URLField(default='https://www.litres.ru/')
    author = models.ForeignKey('Author',
                               related_name='books',
                               on_delete=models.CASCADE)

    publishing_house = models.ForeignKey('PublishingHouse',
                                         related_name='books',
                                         on_delete=models.CASCADE)

    book_cover = models.ImageField(upload_to="covers/", default='404')

    def __str__(self):
        return self.title

class Comment(models.Model):
    user_name = models.CharField('Name',
                                 max_length=120)
    text = models.TextField('Comment',
                            max_length=10000)
    parent = models.ForeignKey('self',
                               related_name='children',
                               on_delete=models.SET_NULL,
                               blank=True,
                               null=True)

    book = models.ForeignKey(Book,
                             on_delete=models.CASCADE,
                             related_name='comments')

    def __str__(self):
        return self.text
