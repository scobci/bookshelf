from rest_framework import permissions

from .models import Author

class AuthorPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return Author.objects.filter(name=request.user.username)



