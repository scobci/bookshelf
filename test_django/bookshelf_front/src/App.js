import React, {Component} from 'react';
import './App.css';

const SEARCH_TAB = [
  { name: "Books", zip: "books" },
  { name: "Authors", zip: "authors" },
  { name: "Publishing Houses", zip: "publishing_houses" }
];

class App extends Component {
  constructor() {
    super();
    this.state = {
      activeTab: 0,
    };
  }
  render() {
    const activeTab = this.state.activeTab
    return (
      <div className="App">
        {SEARCH_TAB.map((tab, index) => (
          <button
            key={index}
            onClick={() => {
              this.setState({ activeTab: index });
            }}
          >
              {tab.name}
          </button>
        ))}
        <ElementDisplay
          key={activeTab}
          zip={SEARCH_TAB[activeTab].zip}
        />
      </div>
    );
  }
}

class ElementDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    fetch("api/" + this.props.zip)
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    if(this.props.zip === "books")
      return renderBooks(this.state.data)
    else if(this.props.zip === "authors")
      return renderAuthor(this.state.data)
    else if(this.props.zip === "publishing_houses")
      return renderPH(this.state.data)
  }
}
function renderBooks(books) {
    return (
      <ul>
        {books.map(book => {
          return (
            <li key={book.id}>
              {book.title} <br/>
              {book.annotation}
              <br/>
            </li>
          );
        })}
      </ul>
    );
}

function renderAuthor(authors) {
    return (
      <ul>
        {authors.map(author => {
          return (
            <li key={author.id}>
              {author.name} <br/>
              {author.date_of_birth}
              <br/>
            </li>
          );
        })}
      </ul>
    );
}

function renderPH(phs) {
    return (
      <ul>
        {phs.map(ph => {
          return (
            <li key={ph.id}>
              {ph.name} <br/>
              {ph.address}
              <br/>
            </li>
          );
        })}
      </ul>
    );
}
export default App;
